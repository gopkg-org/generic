package generic

// Uniq remove duplicate entries in list.
func Uniq[T comparable](v []T) []T {
	tmp := make([]T, 0)

	for _, value := range v {
		if !Has(value, tmp...) {
			tmp = append(tmp, value)
		}
	}

	return tmp
}

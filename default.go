package generic

// Default takes two values.
// If the first value is not empty, it will be returned, else the second value will be returned.
func Default[T any](v1, v2 T) T {
	return Ternary(IsSet(v1), v1, v2)
}

package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestHas(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(HasSuite))
}

type HasSuite struct {
	suite.Suite
}

func (suite *HasSuite) TestBytes() {
	suite.True(generic.Has(byte(0x62), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	suite.False(generic.Has(byte(0x65), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	suite.False(generic.Has(byte(0)))
}

func (suite *HasSuite) TestComplex64() {
	suite.True(generic.Has(complex64(2.8), complex64(1.9), complex64(2.8), complex64(3.7)))
	suite.False(generic.Has(complex64(5), complex64(1.9), complex64(2.8), complex64(3.7)))
	suite.False(generic.Has(complex64(0)))
}

func (suite *HasSuite) TestComplex128() {
	suite.True(generic.Has(complex128(2.8), complex128(1.9), complex128(2.8), complex128(3.7)))
	suite.False(generic.Has(complex128(5), complex128(1.9), complex128(2.8), complex128(3.7)))
	suite.False(generic.Has(complex128(0)))
}

func (suite *HasSuite) TestFloat32() {
	suite.True(generic.Has(float32(2.8), float32(1.9), float32(2.8), float32(3.7)))
	suite.False(generic.Has(float32(5), float32(1.9), float32(2.8), float32(3.7)))
	suite.False(generic.Has(float32(0)))
}

func (suite *HasSuite) TestFloat64() {
	suite.True(generic.Has(2.8, 1.9, 2.8, 3.7))
	suite.False(generic.Has(float64(5), 1.9, 2.8, 3.7))
	suite.False(generic.Has(float64(0)))
}

func (suite *HasSuite) TestInt() {
	suite.True(generic.Has(2, 1, 2, 3))
	suite.False(generic.Has(5, 1, 2, 3))
	suite.False(generic.Has(0))
}

func (suite *HasSuite) TestInt8() {
	suite.True(generic.Has(int8(2), int8(1), int8(2), int8(3)))
	suite.False(generic.Has(int8(5), int8(1), int8(2), int8(3)))
	suite.False(generic.Has(int8(0)))
}

func (suite *HasSuite) TestInt16() {
	suite.True(generic.Has(int16(2), int16(1), int16(2), int16(3)))
	suite.False(generic.Has(int16(5), int16(1), int16(2), int16(3)))
	suite.False(generic.Has(int16(0)))
}

func (suite *HasSuite) TestInt32() {
	suite.True(generic.Has(int32(2), int32(1), int32(2), int32(3)))
	suite.False(generic.Has(int32(5), int32(1), int32(2), int32(3)))
	suite.False(generic.Has(int32(0)))
}

func (suite *HasSuite) TestInt64() {
	suite.True(generic.Has(int64(2), int64(1), int64(2), int64(3)))
	suite.False(generic.Has(int64(5), int64(1), int64(2), int64(3)))
	suite.False(generic.Has(int64(0)))
}

func (suite *HasSuite) TestRunes() {
	suite.True(generic.Has(rune(0x62), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	suite.False(generic.Has(rune(0x65), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	suite.False(generic.Has(rune(0)))
}

func (suite *HasSuite) TestStrings() {
	suite.True(generic.Has("value2", "value1", "value2", "value3"))
	suite.False(generic.Has("value5", "value1", "value2", "value3"))
	suite.False(generic.Has("value"))
}

func (suite *HasSuite) TestUint() {
	suite.True(generic.Has(uint(2), uint(1), uint(2), uint(3)))
	suite.False(generic.Has(uint(5), uint(1), uint(2), uint(3)))
	suite.False(generic.Has(uint(0)))
}

func (suite *HasSuite) TestUint8() {
	suite.True(generic.Has(uint8(2), uint8(1), uint8(2), uint8(3)))
	suite.False(generic.Has(uint8(5), uint8(1), uint8(2), uint8(3)))
	suite.False(generic.Has(uint8(0)))
}

func (suite *HasSuite) TestUint16() {
	suite.True(generic.Has(uint16(2), uint16(1), uint16(2), uint16(3)))
	suite.False(generic.Has(uint16(5), uint16(1), uint16(2), uint16(3)))
	suite.False(generic.Has(uint16(0)))
}

func (suite *HasSuite) TestUint32() {
	suite.True(generic.Has(uint32(2), uint32(1), uint32(2), uint32(3)))
	suite.False(generic.Has(uint32(5), uint32(1), uint32(2), uint32(3)))
	suite.False(generic.Has(uint32(0)))
}

func (suite *HasSuite) TestUint64() {
	suite.True(generic.Has(uint64(2), uint64(1), uint64(2), uint64(3)))
	suite.False(generic.Has(uint64(5), uint64(1), uint64(2), uint64(3)))
	suite.False(generic.Has(uint64(0)))
}

func (suite *HasSuite) TestUintPtr() {
	suite.True(generic.Has(uintptr(2), uintptr(1), uintptr(2), uintptr(3)))
	suite.False(generic.Has(uintptr(5), uintptr(1), uintptr(2), uintptr(3)))
	suite.False(generic.Has(uintptr(0)))
}

func ExampleHas_bytes() {
	fmt.Println(generic.Has(byte(0x62), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	fmt.Println(generic.Has(byte(0x65), byte(0x61), byte(0x62), byte(0x63), byte(0x64)))
	// Output:
	// true
	// false
}

func ExampleHas_complex64() {
	fmt.Println(generic.Has(complex64(2.8), complex64(1.9), complex64(2.8), complex64(3.7)))
	fmt.Println(generic.Has(complex64(5), complex64(1.9), complex64(2.8), complex64(3.7)))
	// Output:
	// true
	// false
}

func ExampleHas_complex128() {
	fmt.Println(generic.Has(complex128(2.8), complex128(1.9), complex128(2.8), complex128(3.7)))
	fmt.Println(generic.Has(complex128(5), complex128(1.9), complex128(2.8), complex128(3.7)))
	// Output:
	// true
	// false
}

func ExampleHas_float32() {
	fmt.Println(generic.Has(float32(2.8), float32(1.9), float32(2.8), float32(3.7)))
	fmt.Println(generic.Has(float32(5), float32(1.9), float32(2.8), float32(3.7)))
	// Output:
	// true
	// false
}

func ExampleHas_float64() {
	fmt.Println(generic.Has(2.8, 1.9, 2.8, 3.7))
	fmt.Println(generic.Has(float64(5), 1.9, 2.8, 3.7))
	// Output:
	// true
	// false
}

func ExampleHas_int() {
	fmt.Println(generic.Has(2, 1, 2, 3))
	fmt.Println(generic.Has(5, 1, 2, 3))
	// Output:
	// true
	// false
}

func ExampleHas_int8() {
	fmt.Println(generic.Has(int8(2), int8(1), int8(2), int8(3)))
	fmt.Println(generic.Has(int8(5), int8(1), int8(2), int8(3)))
	// Output:
	// true
	// false
}

func ExampleHas_int16() {
	fmt.Println(generic.Has(int16(2), int16(1), int16(2), int16(3)))
	fmt.Println(generic.Has(int16(5), int16(1), int16(2), int16(3)))
	// Output:
	// true
	// false
}

func ExampleHas_int32() {
	fmt.Println(generic.Has(int32(2), int32(1), int32(2), int32(3)))
	fmt.Println(generic.Has(int32(5), int32(1), int32(2), int32(3)))
	// Output:
	// true
	// false
}

func ExampleHas_int64() {
	fmt.Println(generic.Has(int64(2), int64(1), int64(2), int64(3)))
	fmt.Println(generic.Has(int64(5), int64(1), int64(2), int64(3)))
	// Output:
	// true
	// false
}

func ExampleHas_runes() {
	fmt.Println(generic.Has(rune(0x62), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	fmt.Println(generic.Has(rune(0x65), rune(0x61), rune(0x62), rune(0x63), rune(0x64)))
	// Output:
	// true
	// false
}

func ExampleHas_strings() {
	fmt.Println(generic.Has("value2", "value1", "value2", "value3"))
	fmt.Println(generic.Has("value5", "value1", "value2", "value3"))
	// Output:
	// true
	// false
}

func ExampleHas_uint() {
	fmt.Println(generic.Has(uint(2), uint(1), uint(2), uint(3)))
	fmt.Println(generic.Has(uint(5), uint(1), uint(2), uint(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint8() {
	fmt.Println(generic.Has(uint8(2), uint8(1), uint8(2), uint8(3)))
	fmt.Println(generic.Has(uint8(5), uint8(1), uint8(2), uint8(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint16() {
	fmt.Println(generic.Has(uint16(2), uint16(1), uint16(2), uint16(3)))
	fmt.Println(generic.Has(uint16(5), uint16(1), uint16(2), uint16(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint32() {
	fmt.Println(generic.Has(uint32(2), uint32(1), uint32(2), uint32(3)))
	fmt.Println(generic.Has(uint32(5), uint32(1), uint32(2), uint32(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uint64() {
	fmt.Println(generic.Has(uint64(2), uint64(1), uint64(2), uint64(3)))
	fmt.Println(generic.Has(uint64(5), uint64(1), uint64(2), uint64(3)))
	// Output:
	// true
	// false
}

func ExampleHas_uintPtr() {
	fmt.Println(generic.Has(uintptr(2), uintptr(1), uintptr(2), uintptr(3)))
	fmt.Println(generic.Has(uintptr(5), uintptr(1), uintptr(2), uintptr(3)))
	// Output:
	// true
	// false
}

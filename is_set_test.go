package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestIsSet(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(IsSetSuite))
}

type IsSetSuite struct {
	suite.Suite
}

func (suite *IsSetSuite) TestBytes() {
	suite.Equal(false, generic.IsSet(byte(0x00)))
	suite.Equal(true, generic.IsSet(byte(0x61)))
}

func (suite *IsSetSuite) TestComplex64() {
	suite.Equal(false, generic.IsSet(complex64(0.0)))
	suite.Equal(true, generic.IsSet(complex64(1.9)))
}

func (suite *IsSetSuite) TestComplex128() {
	suite.Equal(false, generic.IsSet(complex128(0.0)))
	suite.Equal(true, generic.IsSet(complex128(1.9)))
}

func (suite *IsSetSuite) TestFloat32() {
	suite.Equal(false, generic.IsSet(float32(0.0)))
	suite.Equal(true, generic.IsSet(float32(1.9)))
}

func (suite *IsSetSuite) TestFloat64() {
	suite.Equal(false, generic.IsSet(0.0))
	suite.Equal(true, generic.IsSet(1.9))
}

func (suite *IsSetSuite) TestInt() {
	suite.Equal(false, generic.IsSet(0))
	suite.Equal(true, generic.IsSet(1))
}

func (suite *IsSetSuite) TestInt8() {
	suite.Equal(false, generic.IsSet(int8(0)))
	suite.Equal(true, generic.IsSet(int8(1)))
}

func (suite *IsSetSuite) TestInt16() {
	suite.Equal(false, generic.IsSet(int16(0)))
	suite.Equal(true, generic.IsSet(int16(1)))
}

func (suite *IsSetSuite) TestInt32() {
	suite.Equal(false, generic.IsSet(int32(0)))
	suite.Equal(true, generic.IsSet(int32(1)))
}

func (suite *IsSetSuite) TestInt64() {
	suite.Equal(false, generic.IsSet(int64(0)))
	suite.Equal(true, generic.IsSet(int64(1)))
}

func (suite *IsSetSuite) TestRunes() {
	suite.Equal(false, generic.IsSet(rune(0x00)))
	suite.Equal(true, generic.IsSet(rune(0x61)))
}

func (suite *IsSetSuite) TestStrings() {
	suite.Equal(false, generic.IsSet(""))
	suite.Equal(true, generic.IsSet("value"))
}

func (suite *IsSetSuite) TestUint() {
	suite.Equal(false, generic.IsSet(uint(0)))
	suite.Equal(true, generic.IsSet(uint(1)))
}

func (suite *IsSetSuite) TestUint8() {
	suite.Equal(false, generic.IsSet(uint8(0)))
	suite.Equal(true, generic.IsSet(uint8(1)))
}

func (suite *IsSetSuite) TestUint16() {
	suite.Equal(false, generic.IsSet(uint16(0)))
	suite.Equal(true, generic.IsSet(uint16(1)))
}

func (suite *IsSetSuite) TestUint32() {
	suite.Equal(false, generic.IsSet(uint32(0)))
	suite.Equal(true, generic.IsSet(uint32(1)))
}

func (suite *IsSetSuite) TestUint64() {
	suite.Equal(false, generic.IsSet(uint64(0)))
	suite.Equal(true, generic.IsSet(uint64(1)))
}

func (suite *IsSetSuite) TestUintPtr() {
	suite.Equal(false, generic.IsSet(uintptr(0)))
	suite.Equal(true, generic.IsSet(uintptr(1)))
}

func ExampleIsSet_bytes() {
	fmt.Println(generic.IsSet(byte(0x00)))
	fmt.Println(generic.IsSet(byte(0x61)))
	// Output:
	// false
	// true
}

func ExampleIsSet_complex64() {
	fmt.Println(generic.IsSet(complex64(0.0)))
	fmt.Println(generic.IsSet(complex64(2.8)))
	// Output:
	// false
	// true
}

func ExampleIsSet_complex128() {
	fmt.Println(generic.IsSet(complex128(0.0)))
	fmt.Println(generic.IsSet(complex128(2.8)))
	// Output:
	// false
	// true
}

func ExampleIsSet_float32() {
	fmt.Println(generic.IsSet(float32(0.0)))
	fmt.Println(generic.IsSet(float32(2.8)))
	// Output:
	// false
	// true
}

func ExampleIsSet_float64() {
	fmt.Println(generic.IsSet(0.0))
	fmt.Println(generic.IsSet(2.8))
	// Output:
	// false
	// true
}

func ExampleIsSet_int() {
	fmt.Println(generic.IsSet(0))
	fmt.Println(generic.IsSet(1))
	// Output:
	// false
	// true
}

func ExampleIsSet_int8() {
	fmt.Println(generic.IsSet(int8(0)))
	fmt.Println(generic.IsSet(int8(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_int16() {
	fmt.Println(generic.IsSet(int16(0)))
	fmt.Println(generic.IsSet(int16(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_int32() {
	fmt.Println(generic.IsSet(int32(0)))
	fmt.Println(generic.IsSet(int32(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_int64() {
	fmt.Println(generic.IsSet(int64(0)))
	fmt.Println(generic.IsSet(int64(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_runes() {
	fmt.Println(generic.IsSet(rune(0x00)))
	fmt.Println(generic.IsSet(rune(0x6e)))
	// Output:
	// false
	// true
}

func ExampleIsSet_strings() {
	fmt.Println(generic.IsSet(""))
	fmt.Println(generic.IsSet("value"))
	// Output:
	// false
	// true
}

func ExampleIsSet_uint() {
	fmt.Println(generic.IsSet(uint(0)))
	fmt.Println(generic.IsSet(uint(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_uint8() {
	fmt.Println(generic.IsSet(uint8(0)))
	fmt.Println(generic.IsSet(uint8(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_uint16() {
	fmt.Println(generic.IsSet(uint16(0)))
	fmt.Println(generic.IsSet(uint16(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_uint32() {
	fmt.Println(generic.IsSet(uint32(0)))
	fmt.Println(generic.IsSet(uint32(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_uint64() {
	fmt.Println(generic.IsSet(uint64(0)))
	fmt.Println(generic.IsSet(uint64(1)))
	// Output:
	// false
	// true
}

func ExampleIsSet_uintPtr() {
	fmt.Println(generic.IsSet(uintptr(0)))
	fmt.Println(generic.IsSet(uintptr(1)))
	// Output:
	// false
	// true
}

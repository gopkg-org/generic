package generic

import "reflect"

// Make empty slice
func Make[T any](sizes ...int) T {
	to := reflect.TypeOf(new(T)).Elem()

	if to.Kind() != reflect.Slice {
		panic("can't make no slice type")
	}

	var length, capacity = 0, 0
	if len(sizes) >= 2 {
		length, capacity = sizes[0], sizes[1]
	} else if len(sizes) == 1 {
		length, capacity = sizes[0], sizes[0]
	}

	return reflect.MakeSlice(to, length, capacity).Interface().(T)
}

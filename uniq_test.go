package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestUniq(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(UniqSuite))
}

type UniqSuite struct {
	suite.Suite
}

func (suite *UniqSuite) TestBytes() {
	suite.Equal([]byte{0x61, 0x62, 0x63}, generic.Uniq([]byte{0x61, 0x62, 0x61, 0x63}))
}

func (suite *UniqSuite) TestComplex64() {
	suite.Equal([]complex64{1.9, 2.8, 3.7}, generic.Uniq([]complex64{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestComplex128() {
	suite.Equal([]complex128{1.9, 2.8, 3.7}, generic.Uniq([]complex128{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestFloat32() {
	suite.Equal([]float32{1.9, 2.8, 3.7}, generic.Uniq([]float32{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestFloat64() {
	suite.Equal([]float64{1.9, 2.8, 3.7}, generic.Uniq([]float64{1.9, 2.8, 1.9, 3.7}))
}

func (suite *UniqSuite) TestInt() {
	suite.Equal([]int{1, 2, 3}, generic.Uniq([]int{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt8() {
	suite.Equal([]int8{1, 2, 3}, generic.Uniq([]int8{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt16() {
	suite.Equal([]int16{1, 2, 3}, generic.Uniq([]int16{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt32() {
	suite.Equal([]int32{1, 2, 3}, generic.Uniq([]int32{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestInt64() {
	suite.Equal([]int64{1, 2, 3}, generic.Uniq([]int64{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestRunes() {
	suite.Equal([]rune{0x61, 0x62, 0x63}, generic.Uniq([]rune{0x61, 0x62, 0x61, 0x63}))
}

func (suite *UniqSuite) TestStrings() {
	suite.Equal([]string{"v1", "v2", "v3"}, generic.Uniq([]string{"v1", "v2", "v1", "v3"}))
}

func (suite *UniqSuite) TestUint() {
	suite.Equal([]uint{1, 2, 3}, generic.Uniq([]uint{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint8() {
	suite.Equal([]uint8{1, 2, 3}, generic.Uniq([]uint8{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint16() {
	suite.Equal([]uint16{1, 2, 3}, generic.Uniq([]uint16{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint32() {
	suite.Equal([]uint32{1, 2, 3}, generic.Uniq([]uint32{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUint64() {
	suite.Equal([]uint64{1, 2, 3}, generic.Uniq([]uint64{1, 2, 1, 3}))
}

func (suite *UniqSuite) TestUintPtr() {
	suite.Equal([]uintptr{1, 2, 3}, generic.Uniq([]uintptr{1, 2, 1, 3}))
}

func ExampleUniq_bytes() {
	fmt.Printf("%#v\n", generic.Uniq([]byte{0x61, 0x62, 0x61, 0x63}))
	// Output: []byte{0x61, 0x62, 0x63}
}

func ExampleUniq_complex64() {
	fmt.Printf("%v\n", generic.Uniq([]complex64{1.9, 2.8, 1.9, 3.7}))
	// Output: [(1.9+0i) (2.8+0i) (3.7+0i)]
}

func ExampleUniq_complex128() {
	fmt.Printf("%v\n", generic.Uniq([]complex128{1.9, 2.8, 1.9, 3.7}))
	// Output: [(1.9+0i) (2.8+0i) (3.7+0i)]
}

func ExampleUniq_float32() {
	fmt.Printf("%v\n", generic.Uniq([]float32{1.9, 2.8, 1.9, 3.7}))
	// Output: [1.9 2.8 3.7]
}

func ExampleUniq_float64() {
	fmt.Printf("%v\n", generic.Uniq([]float64{1.9, 2.8, 1.9, 3.7}))
	// Output: [1.9 2.8 3.7]
}

func ExampleUniq_int() {
	fmt.Printf("%v\n", generic.Uniq([]int{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int8() {
	fmt.Printf("%v\n", generic.Uniq([]int8{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int16() {
	fmt.Printf("%v\n", generic.Uniq([]int16{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int32() {
	fmt.Printf("%v\n", generic.Uniq([]int32{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_int64() {
	fmt.Printf("%v\n", generic.Uniq([]int64{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_runes() {
	fmt.Printf("%#x\n", generic.Uniq([]rune{0x61, 0x62, 0x61, 0x63}))
	// Output: [0x61 0x62 0x63]
}

func ExampleUniq_strings() {
	fmt.Printf("%v\n", generic.Uniq([]string{"v1", "v2", "v1", "v3"}))
	// Output: [v1 v2 v3]
}

func ExampleUniq_uint() {
	fmt.Printf("%v\n", generic.Uniq([]uint{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint8() {
	fmt.Printf("%v\n", generic.Uniq([]uint8{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint16() {
	fmt.Printf("%v\n", generic.Uniq([]uint16{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint32() {
	fmt.Printf("%v\n", generic.Uniq([]uint32{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uint64() {
	fmt.Printf("%v\n", generic.Uniq([]uint64{1, 2, 1, 3}))
	// Output: [1 2 3]
}

func ExampleUniq_uintPtr() {
	fmt.Printf("%v\n", generic.Uniq([]uintptr{1, 2, 1, 3}))
	// Output: [1 2 3]
}

package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestPointer(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(PointerSuite))
}

type PointerSuite struct {
	suite.Suite
}

func (suite *PointerSuite) TestBool() {
	test1 := generic.Pointer(true)
	suite.IsType((*bool)(nil), test1)
	suite.Equal(true, *test1)

	test2 := generic.Pointer(false)
	suite.IsType((*bool)(nil), test2)
	suite.Equal(false, *test2)
}

func (suite *PointerSuite) TestByte() {
	test1 := generic.Pointer(byte(0x61))
	suite.IsType((*uint8)(nil), test1)
	suite.Equal(uint8(97), *test1)

	test2 := generic.Pointer(byte(0))
	suite.IsType((*uint8)(nil), test2)
	suite.Equal(uint8(0), *test2)
}

func (suite *PointerSuite) TestComplex64() {
	test1 := generic.Pointer(complex64(123.987))
	suite.IsType((*complex64)(nil), test1)
	suite.Equal(complex64(123.987), *test1)

	test2 := generic.Pointer(complex64(0.123))
	suite.IsType((*complex64)(nil), test2)
	suite.Equal(complex64(0.123), *test2)
}

func (suite *PointerSuite) TestComplex128() {
	test1 := generic.Pointer(complex128(123.987))
	suite.IsType((*complex128)(nil), test1)
	suite.Equal(complex128(123.987), *test1)

	test2 := generic.Pointer(complex128(0.123))
	suite.IsType((*complex128)(nil), test2)
	suite.Equal(complex128(0.123), *test2)
}

func (suite *PointerSuite) TestFloat32() {
	test1 := generic.Pointer(float32(123.987))
	suite.IsType((*float32)(nil), test1)
	suite.Equal(float32(123.987), *test1)

	test2 := generic.Pointer(float32(0.123))
	suite.IsType((*float32)(nil), test2)
	suite.Equal(float32(0.123), *test2)
}

func (suite *PointerSuite) TestFloat64() {
	test1 := generic.Pointer(123.987)
	suite.IsType((*float64)(nil), test1)
	suite.Equal(123.987, *test1)

	test2 := generic.Pointer(0.123)
	suite.IsType((*float64)(nil), test2)
	suite.Equal(0.123, *test2)
}

func (suite *PointerSuite) TestInt() {
	test1 := generic.Pointer(1)
	suite.IsType((*int)(nil), test1)
	suite.Equal(1, *test1)

	test2 := generic.Pointer(0)
	suite.IsType((*int)(nil), test2)
	suite.Equal(0, *test2)
}

func (suite *PointerSuite) TestInt8() {
	test1 := generic.Pointer(int8(1))
	suite.IsType((*int8)(nil), test1)
	suite.Equal(int8(1), *test1)

	test2 := generic.Pointer(int8(0))
	suite.IsType((*int8)(nil), test2)
	suite.Equal(int8(0), *test2)
}

func (suite *PointerSuite) TestInt16() {
	test1 := generic.Pointer(int16(1))
	suite.IsType((*int16)(nil), test1)
	suite.Equal(int16(1), *test1)

	test2 := generic.Pointer(int16(0))
	suite.IsType((*int16)(nil), test2)
	suite.Equal(int16(0), *test2)
}

func (suite *PointerSuite) TestInt32() {
	test1 := generic.Pointer(int32(1))
	suite.IsType((*int32)(nil), test1)
	suite.Equal(int32(1), *test1)

	test2 := generic.Pointer(int32(0))
	suite.IsType((*int32)(nil), test2)
	suite.Equal(int32(0), *test2)
}

func (suite *PointerSuite) TestInt64() {
	test1 := generic.Pointer(int64(1))
	suite.IsType((*int64)(nil), test1)
	suite.Equal(int64(1), *test1)

	test2 := generic.Pointer(int64(0))
	suite.IsType((*int64)(nil), test2)
	suite.Equal(int64(0), *test2)
}

func (suite *PointerSuite) TestRune() {
	test1 := generic.Pointer(rune(0x61))
	suite.IsType((*int32)(nil), test1)
	suite.Equal(int32(97), *test1)

	test2 := generic.Pointer(rune(0))
	suite.IsType((*int32)(nil), test2)
	suite.Equal(int32(0), *test2)
}

func (suite *PointerSuite) TestString() {
	test1 := generic.Pointer("test")
	suite.IsType((*string)(nil), test1)
	suite.Equal("test", *test1)

	test2 := generic.Pointer("")
	suite.IsType((*string)(nil), test2)
	suite.Equal("", *test2)
}

func (suite *PointerSuite) TestUint() {
	test1 := generic.Pointer(uint(1))
	suite.IsType((*uint)(nil), test1)
	suite.Equal(uint(1), *test1)

	test2 := generic.Pointer(uint(0))
	suite.IsType((*uint)(nil), test2)
	suite.Equal(uint(0), *test2)
}

func (suite *PointerSuite) TestUint8() {
	test1 := generic.Pointer(uint8(1))
	suite.IsType((*uint8)(nil), test1)
	suite.Equal(uint8(1), *test1)

	test2 := generic.Pointer(uint8(0))
	suite.IsType((*uint8)(nil), test2)
	suite.Equal(uint8(0), *test2)
}

func (suite *PointerSuite) TestUint16() {
	test1 := generic.Pointer(uint16(1))
	suite.IsType((*uint16)(nil), test1)
	suite.Equal(uint16(1), *test1)

	test2 := generic.Pointer(uint16(0))
	suite.IsType((*uint16)(nil), test2)
	suite.Equal(uint16(0), *test2)
}

func (suite *PointerSuite) TestUint32() {
	test1 := generic.Pointer(uint32(1))
	suite.IsType((*uint32)(nil), test1)
	suite.Equal(uint32(1), *test1)

	test2 := generic.Pointer(uint32(0))
	suite.IsType((*uint32)(nil), test2)
	suite.Equal(uint32(0), *test2)
}

func (suite *PointerSuite) TestUint64() {
	test1 := generic.Pointer(uint64(1))
	suite.IsType((*uint64)(nil), test1)
	suite.Equal(uint64(1), *test1)

	test2 := generic.Pointer(uint64(0))
	suite.IsType((*uint64)(nil), test2)
	suite.Equal(uint64(0), *test2)
}

func (suite *PointerSuite) TestUintPtr() {
	test1 := generic.Pointer(uintptr(1))
	suite.IsType((*uintptr)(nil), test1)
	suite.Equal(uintptr(1), *test1)

	test2 := generic.Pointer(uintptr(0))
	suite.IsType((*uintptr)(nil), test2)
	suite.Equal(uintptr(0), *test2)
}

func ExamplePointer_bool() {
	ptr := generic.Pointer(true)
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *bool = true
}

func ExamplePointer_byte() {
	ptr := generic.Pointer(byte(0x61))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint8 = 97
}

func ExamplePointer_complex64() {
	ptr := generic.Pointer(complex64(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *complex64 = (123.987+0i)
}

func ExamplePointer_complex128() {
	ptr := generic.Pointer(complex128(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *complex128 = (123.987+0i)
}

func ExamplePointer_float32() {
	ptr := generic.Pointer(float32(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *float32 = 123.987
}

func ExamplePointer_float64() {
	ptr := generic.Pointer(float64(123.987))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *float64 = 123.987
}

func ExamplePointer_int() {
	ptr := generic.Pointer(int(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int = 123
}

func ExamplePointer_int8() {
	ptr := generic.Pointer(int8(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int8 = 123
}

func ExamplePointer_int16() {
	ptr := generic.Pointer(int16(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int16 = 123
}

func ExamplePointer_int32() {
	ptr := generic.Pointer(int32(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int32 = 123
}

func ExamplePointer_int64() {
	ptr := generic.Pointer(int64(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int64 = 123
}

func ExamplePointer_rune() {
	ptr := generic.Pointer(rune(0x61))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *int32 = 97
}

func ExamplePointer_string() {
	ptr := generic.Pointer("value")
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *string = value
}

func ExamplePointer_uint() {
	ptr := generic.Pointer(uint(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint = 123
}

func ExamplePointer_uint8() {
	ptr := generic.Pointer(uint8(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint8 = 123
}

func ExamplePointer_uint16() {
	ptr := generic.Pointer(uint16(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint16 = 123
}

func ExamplePointer_uint32() {
	ptr := generic.Pointer(uint32(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint32 = 123
}

func ExamplePointer_uint64() {
	ptr := generic.Pointer(uint64(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uint64 = 123
}

func ExamplePointer_uintPtr() {
	ptr := generic.Pointer(uintptr(123))
	fmt.Printf("%T = %v\n", ptr, *ptr)
	// Output: *uintptr = 123
}

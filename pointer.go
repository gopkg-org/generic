package generic

// Pointer return value pointer.
func Pointer[T any](v T) *T {
	return &v
}

package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestZero(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(ZeroSuite))
}

type ZeroSuite struct {
	suite.Suite
}

func (suite *ZeroSuite) TestBytes() {
	suite.Equal([]byte(nil), generic.Zero[[]byte]())
}

func (suite *ZeroSuite) TestComplex64() {
	suite.Equal(complex64(0), generic.Zero[complex64]())
}

func (suite *ZeroSuite) TestComplex128() {
	suite.Equal(complex128(0), generic.Zero[complex128]())
}

func (suite *ZeroSuite) TestFloat32() {
	suite.Equal(float32(0), generic.Zero[float32]())
}

func (suite *ZeroSuite) TestFloat64() {
	suite.Equal(float64(0), generic.Zero[float64]())
}

func (suite *ZeroSuite) TestInt() {
	suite.Equal(0, generic.Zero[int]())
}

func (suite *ZeroSuite) TestInt8() {
	suite.Equal(int8(0), generic.Zero[int8]())
}

func (suite *ZeroSuite) TestInt16() {
	suite.Equal(int16(0), generic.Zero[int16]())
}

func (suite *ZeroSuite) TestInt32() {
	suite.Equal(int32(0), generic.Zero[int32]())
}

func (suite *ZeroSuite) TestInt64() {
	suite.Equal(int64(0), generic.Zero[int64]())
}

func (suite *ZeroSuite) TestStrings() {
	suite.Equal("", generic.Zero[string]())
}

func (suite *ZeroSuite) TestUint() {
	suite.Equal(uint(0), generic.Zero[uint]())
}

func (suite *ZeroSuite) TestUint8() {
	suite.Equal(uint8(0), generic.Zero[uint8]())
}

func (suite *ZeroSuite) TestUint16() {
	suite.Equal(uint16(0), generic.Zero[uint16]())
}

func (suite *ZeroSuite) TestUint32() {
	suite.Equal(uint32(0), generic.Zero[uint32]())
}

func (suite *ZeroSuite) TestUint64() {
	suite.Equal(uint64(0), generic.Zero[uint64]())
}

func (suite *ZeroSuite) TestUintPtr() {
	suite.Equal(uintptr(0), generic.Zero[uintptr]())
}

func ExampleZero_bytes() {
	fmt.Printf("%#v\n", generic.Zero[[]byte]())
	// Output: []byte(nil)
}

func ExampleZero_complex64() {
	zero := generic.Zero[complex64]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: complex64 => (0+0i)
}

func ExampleZero_complex128() {
	zero := generic.Zero[complex128]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: complex128 => (0+0i)
}

func ExampleZero_float32() {
	zero := generic.Zero[float32]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: float32 => 0
}

func ExampleZero_float64() {
	zero := generic.Zero[float64]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: float64 => 0
}

func ExampleZero_int() {
	zero := generic.Zero[int]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: int => 0
}

func ExampleZero_int8() {
	zero := generic.Zero[int8]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: int8 => 0
}

func ExampleZero_int16() {
	zero := generic.Zero[int16]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: int16 => 0
}

func ExampleZero_int32() {
	zero := generic.Zero[int32]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: int32 => 0
}

func ExampleZero_int64() {
	zero := generic.Zero[int64]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: int64 => 0
}

func ExampleZero_strings() {
	fmt.Printf("%#v\n", generic.Zero[string]())
	// Output: ""
}

func ExampleZero_uint() {
	zero := generic.Zero[uint]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: uint => 0
}

func ExampleZero_uint8() {
	zero := generic.Zero[uint8]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: uint8 => 0
}

func ExampleZero_uint16() {
	zero := generic.Zero[uint16]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: uint16 => 0
}

func ExampleZero_uint32() {
	zero := generic.Zero[uint32]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: uint32 => 0
}

func ExampleZero_uint64() {
	zero := generic.Zero[uint64]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: uint64 => 0
}

func ExampleZero_uintPtr() {
	zero := generic.Zero[uintptr]()
	fmt.Printf("%T => %v\n", zero, zero)
	// Output: uintptr => 0
}

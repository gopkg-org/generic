package generic_test

import (
	"fmt"
	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
	"testing"
)

func TestCompact(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(CompactSuite))
}

type CompactSuite struct {
	suite.Suite
}

func (suite *CompactSuite) TestBytes() {
	suite.Equal([]byte{0x61, 0x62, 0x63, 0x64}, generic.Compact([]byte{0x00, 0x61, 0x62, 0x63, 0x64}))
	suite.Equal([]byte{0x61, 0x62, 0x63, 0x64}, generic.Compact([]byte{0x61, 0x62, 0x63, 0x64, 0x00}))
	suite.Equal([]byte{0x61, 0x62, 0x63, 0x64}, generic.Compact([]byte{0x61, 0x62, 0x00, 0x63, 0x64}))
	suite.Nil(generic.Compact[byte](nil))
}

func (suite *CompactSuite) TestComplex64() {
	suite.Equal([]complex64{1.9, 2.8, 3.7, 0.5}, generic.Compact([]complex64{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]complex64{0.5, 3.7, 2.8, 1.9}, generic.Compact([]complex64{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]complex64{0.5, 3.7, 1.9, 2.8}, generic.Compact([]complex64{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(generic.Compact[complex64](nil))
}

func (suite *CompactSuite) TestComplex128() {
	suite.Equal([]complex128{1.9, 2.8, 3.7, 0.5}, generic.Compact([]complex128{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]complex128{0.5, 3.7, 2.8, 1.9}, generic.Compact([]complex128{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]complex128{0.5, 3.7, 1.9, 2.8}, generic.Compact([]complex128{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(generic.Compact[complex128](nil))
}

func (suite *CompactSuite) TestFloat32() {
	suite.Equal([]float32{1.9, 2.8, 3.7, 0.5}, generic.Compact([]float32{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]float32{0.5, 3.7, 2.8, 1.9}, generic.Compact([]float32{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]float32{0.5, 3.7, 1.9, 2.8}, generic.Compact([]float32{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(generic.Compact[float32](nil))
}

func (suite *CompactSuite) TestFloat64() {
	suite.Equal([]float64{1.9, 2.8, 3.7, 0.5}, generic.Compact([]float64{0, 1.9, 2.8, 3.7, 0.5}))
	suite.Equal([]float64{0.5, 3.7, 2.8, 1.9}, generic.Compact([]float64{0.5, 3.7, 2.8, 1.9, 0}))
	suite.Equal([]float64{0.5, 3.7, 1.9, 2.8}, generic.Compact([]float64{0.5, 3.7, 0, 1.9, 2.8}))
	suite.Nil(generic.Compact[float64](nil))
}

func (suite *CompactSuite) TestInt() {
	suite.Equal([]int{1, 2, 3}, generic.Compact([]int{0, 1, 2, 3}))
	suite.Equal([]int{3, 2, 1}, generic.Compact([]int{3, 2, 1, 0}))
	suite.Equal([]int{3, 2, 1, 1, 2, 3}, generic.Compact([]int{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[int](nil))
}

func (suite *CompactSuite) TestInt8() {
	suite.Equal([]int8{1, 2, 3}, generic.Compact([]int8{0, 1, 2, 3}))
	suite.Equal([]int8{3, 2, 1}, generic.Compact([]int8{3, 2, 1, 0}))
	suite.Equal([]int8{3, 2, 1, 1, 2, 3}, generic.Compact([]int8{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[int8](nil))
}

func (suite *CompactSuite) TestInt16() {
	suite.Equal([]int16{1, 2, 3}, generic.Compact([]int16{0, 1, 2, 3}))
	suite.Equal([]int16{3, 2, 1}, generic.Compact([]int16{3, 2, 1, 0}))
	suite.Equal([]int16{3, 2, 1, 1, 2, 3}, generic.Compact([]int16{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[int16](nil))
}

func (suite *CompactSuite) TestInt32() {
	suite.Equal([]int32{1, 2, 3}, generic.Compact([]int32{0, 1, 2, 3}))
	suite.Equal([]int32{3, 2, 1}, generic.Compact([]int32{3, 2, 1, 0}))
	suite.Equal([]int32{3, 2, 1, 1, 2, 3}, generic.Compact([]int32{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[int32](nil))

}

func (suite *CompactSuite) TestInt64() {
	suite.Equal([]int64{1, 2, 3}, generic.Compact([]int64{0, 1, 2, 3}))
	suite.Equal([]int64{3, 2, 1}, generic.Compact([]int64{3, 2, 1, 0}))
	suite.Equal([]int64{3, 2, 1, 1, 2, 3}, generic.Compact([]int64{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[int64](nil))
}

func (suite *CompactSuite) TestRunes() {
	suite.Equal([]rune{0x61, 0x62, 0x63, 0x64}, generic.Compact([]rune{0x00, 0x61, 0x62, 0x63, 0x64}))
	suite.Equal([]rune{0x61, 0x62, 0x63, 0x64}, generic.Compact([]rune{0x61, 0x62, 0x63, 0x64, 0x00}))
	suite.Equal([]rune{0x61, 0x62, 0x63, 0x64}, generic.Compact([]rune{0x61, 0x62, 0x00, 0x63, 0x64}))
	suite.Nil(generic.Compact[rune](nil))
}

func (suite *CompactSuite) TestStrings() {
	suite.Equal([]string{"foo", "bar"}, generic.Compact([]string{"", "foo", "bar"}))
	suite.Equal([]string{"foo", "bar"}, generic.Compact([]string{"foo", "bar", ""}))
	suite.Equal([]string{"foo", "bar"}, generic.Compact([]string{"foo", "", "bar"}))
	suite.Nil(generic.Compact[string](nil))
}

func (suite *CompactSuite) TestUint() {
	suite.Equal([]uint{1, 2, 3}, generic.Compact([]uint{0, 1, 2, 3}))
	suite.Equal([]uint{3, 2, 1}, generic.Compact([]uint{3, 2, 1, 0}))
	suite.Equal([]uint{3, 2, 1, 1, 2, 3}, generic.Compact([]uint{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[uint](nil))
}

func (suite *CompactSuite) TestUint8() {
	suite.Equal([]uint8{1, 2, 3}, generic.Compact([]uint8{0, 1, 2, 3}))
	suite.Equal([]uint8{3, 2, 1}, generic.Compact([]uint8{3, 2, 1, 0}))
	suite.Equal([]uint8{3, 2, 1, 1, 2, 3}, generic.Compact([]uint8{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[uint8](nil))
}

func (suite *CompactSuite) TestUint16() {
	suite.Equal([]uint16{1, 2, 3}, generic.Compact([]uint16{0, 1, 2, 3}))
	suite.Equal([]uint16{3, 2, 1}, generic.Compact([]uint16{3, 2, 1, 0}))
	suite.Equal([]uint16{3, 2, 1, 1, 2, 3}, generic.Compact([]uint16{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[uint16](nil))
}

func (suite *CompactSuite) TestUint32() {
	suite.Equal([]uint32{1, 2, 3}, generic.Compact([]uint32{0, 1, 2, 3}))
	suite.Equal([]uint32{3, 2, 1}, generic.Compact([]uint32{3, 2, 1, 0}))
	suite.Equal([]uint32{3, 2, 1, 1, 2, 3}, generic.Compact([]uint32{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[uint32](nil))
}

func (suite *CompactSuite) TestUint64() {
	suite.Equal([]uint64{1, 2, 3}, generic.Compact([]uint64{0, 1, 2, 3}))
	suite.Equal([]uint64{3, 2, 1}, generic.Compact([]uint64{3, 2, 1, 0}))
	suite.Equal([]uint64{3, 2, 1, 1, 2, 3}, generic.Compact([]uint64{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[uint64](nil))
}

func (suite *CompactSuite) TestUintPtr() {
	suite.Equal([]uintptr{1, 2, 3}, generic.Compact([]uintptr{0, 1, 2, 3}))
	suite.Equal([]uintptr{3, 2, 1}, generic.Compact([]uintptr{3, 2, 1, 0}))
	suite.Equal([]uintptr{3, 2, 1, 1, 2, 3}, generic.Compact([]uintptr{3, 2, 1, 0, 1, 2, 3}))
	suite.Nil(generic.Compact[uintptr](nil))
}

func ExampleCompact_bytes() {
	fmt.Printf("%#v\n", generic.Compact([]byte{0x00, 0x61, 0x62, 0x00, 0x63, 0x64, 0x00}))
	// Output: []byte{0x61, 0x62, 0x63, 0x64}
}

func ExampleCompact_complex64() {
	v := generic.Compact([]complex64{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex64, [(1.9+0i) (2.8+0i) (3.7+0i) (0.5+0i)]
}

func ExampleCompact_complex128() {
	v := generic.Compact([]complex128{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []complex128, [(1.9+0i) (2.8+0i) (3.7+0i) (0.5+0i)]
}

func ExampleCompact_float32() {
	v := generic.Compact([]float32{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float32, [1.9 2.8 3.7 0.5]
}

func ExampleCompact_float64() {
	v := generic.Compact([]float64{0, 1.9, 2.8, 3.7, 0.5})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []float64, [1.9 2.8 3.7 0.5]
}

func ExampleCompact_int() {
	v := generic.Compact([]int{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int, [1 2 3 4]
}

func ExampleCompact_int8() {
	v := generic.Compact([]int8{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int8, [1 2 3 4]
}

func ExampleCompact_int16() {
	v := generic.Compact([]int16{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int16, [1 2 3 4]
}

func ExampleCompact_int32() {
	v := generic.Compact([]int32{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int32, [1 2 3 4]
}

func ExampleCompact_int64() {
	v := generic.Compact([]int64{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []int64, [1 2 3 4]
}

func ExampleCompact_runes() {
	v := generic.Compact([]rune{0x00, 0x61, 0x62, 0x00, 0x63, 0x64, 0x00})
	fmt.Printf("%T, %#x\n", v, v)
	// Output: []int32, [0x61 0x62 0x63 0x64]
}

func ExampleCompact_strings() {
	v := generic.Compact([]string{"", "foo", "", "bar", ""})
	fmt.Printf("%T, %q\n", v, v)
	// Output: []string, ["foo" "bar"]
}

func ExampleCompact_uint() {
	v := generic.Compact([]uint{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint, [1 2 3 4]
}

func ExampleCompact_uint8() {
	v := generic.Compact([]uint8{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint8, [1 2 3 4]
}

func ExampleCompact_uint16() {
	v := generic.Compact([]uint16{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint16, [1 2 3 4]
}

func ExampleCompact_uint32() {
	v := generic.Compact([]uint32{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint32, [1 2 3 4]
}

func ExampleCompact_uint64() {
	v := generic.Compact([]uint64{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uint64, [1 2 3 4]
}

func ExampleCompact_uintPtr() {
	v := generic.Compact([]uintptr{0, 1, 2, 0, 3, 4, 0})
	fmt.Printf("%T, %v\n", v, v)
	// Output: []uintptr, [1 2 3 4]
}

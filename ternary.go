package generic

// Ternary takes a test boolean value, and two any values.
// If the test is true, the first value will be returned.
// If the test is empty, the second value will be returned.
func Ternary[T any](v bool, vTrue, vFalse T) T {
	if v {
		return vTrue
	}

	return vFalse
}

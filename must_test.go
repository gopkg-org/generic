package generic_test

import (
	"errors"
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestMust(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(MustSuite))
}

type MustSuite struct {
	suite.Suite
}

func (suite *MustSuite) TestBytes() {
	suite.PanicsWithError("string", func() { generic.Must([]byte("abc"), errors.New("string")) })
	suite.Equal([]byte("abc"), generic.Must([]byte("abc"), nil))
}

func (suite *MustSuite) TestComplex64() {
	suite.PanicsWithError("string", func() { generic.Must(complex64(123.98), errors.New("string")) })
	suite.Equal(complex64(123.98), generic.Must(complex64(123.98), nil))
}

func (suite *MustSuite) TestComplex128() {
	suite.PanicsWithError("string", func() { generic.Must(complex128(123.98), errors.New("string")) })
	suite.Equal(complex128(123.98), generic.Must(complex128(123.98), nil))
}

func (suite *MustSuite) TestFloat32() {
	suite.PanicsWithError("string", func() { generic.Must(float32(123.98), errors.New("string")) })
	suite.Equal(float32(123.98), generic.Must(float32(123.98), nil))
}

func (suite *MustSuite) TestFloat64() {
	suite.PanicsWithError("string", func() { generic.Must(float64(123.98), errors.New("string")) })
	suite.Equal(float64(123.98), generic.Must(float64(123.98), nil))
}

func (suite *MustSuite) TestInt() {
	suite.PanicsWithError("string", func() { generic.Must(123, errors.New("string")) })
	suite.Equal(123, generic.Must(123, nil))
}

func (suite *MustSuite) TestInt8() {
	suite.PanicsWithError("string", func() { generic.Must(int8(123), errors.New("string")) })
	suite.Equal(int8(123), generic.Must(int8(123), nil))
}

func (suite *MustSuite) TestInt16() {
	suite.PanicsWithError("string", func() { generic.Must(int16(123), errors.New("string")) })
	suite.Equal(int16(123), generic.Must(int16(123), nil))
}

func (suite *MustSuite) TestInt32() {
	suite.PanicsWithError("string", func() { generic.Must(int32(123), errors.New("string")) })
	suite.Equal(int32(123), generic.Must(int32(123), nil))
}

func (suite *MustSuite) TestInt64() {
	suite.PanicsWithError("string", func() { generic.Must(int64(123), errors.New("string")) })
	suite.Equal(int64(123), generic.Must(int64(123), nil))
}

func (suite *MustSuite) TestStrings() {
	suite.PanicsWithError("string", func() { generic.Must("value", errors.New("string")) })
	suite.Equal("value", generic.Must("value", nil))
}

func (suite *MustSuite) TestUint() {
	suite.PanicsWithError("string", func() { generic.Must(uint(123), errors.New("string")) })
	suite.Equal(uint(123), generic.Must(uint(123), nil))
}

func (suite *MustSuite) TestUint8() {
	suite.PanicsWithError("string", func() { generic.Must(uint8(123), errors.New("string")) })
	suite.Equal(uint8(123), generic.Must(uint8(123), nil))
}

func (suite *MustSuite) TestUint16() {
	suite.PanicsWithError("string", func() { generic.Must(uint16(123), errors.New("string")) })
	suite.Equal(uint16(123), generic.Must(uint16(123), nil))
}

func (suite *MustSuite) TestUint32() {
	suite.PanicsWithError("string", func() { generic.Must(uint32(123), errors.New("string")) })
	suite.Equal(uint32(123), generic.Must(uint32(123), nil))
}

func (suite *MustSuite) TestUint64() {
	suite.PanicsWithError("string", func() { generic.Must(uint64(123), errors.New("string")) })
	suite.Equal(uint64(123), generic.Must(uint64(123), nil))
}

func (suite *MustSuite) TestUintPtr() {
	suite.PanicsWithError("string", func() { generic.Must(uintptr(123), errors.New("string")) })
	suite.Equal(uintptr(123), generic.Must(uintptr(123), nil))
}

func ExampleMust_parseComplex() {
	must := generic.Must(strconv.ParseComplex("123.98", 128))
	fmt.Printf("%T => %#v\n", must, must)
	// Output: complex128 => (123.98+0i)
}

func ExampleMust_parseFloat() {
	must := generic.Must(strconv.ParseFloat("123.98", 64))
	fmt.Printf("%T => %#v\n", must, must)
	// Output: float64 => 123.98
}

func ExampleMust_parseInt() {
	must := generic.Must(strconv.ParseInt("123", 10, 0))
	fmt.Printf("%T => %#v\n", must, must)
	// Output: int64 => 123
}

func ExampleMust_parseUint() {
	must := generic.Must(strconv.ParseUint("123", 10, 0))
	fmt.Printf("%T => %#v\n", must, must)
	// Output: uint64 => 0x7b
}

func ExampleMust_parseBool() {
	must := generic.Must(strconv.ParseBool("true"))
	fmt.Printf("%T => %#v\n", must, must)
	// Output: bool => true
}

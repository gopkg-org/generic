package generic

// Compact accepts a list and removes entries with empty values.
func Compact[T any](values []T) []T {
	if values == nil {
		return nil
	}

	list := make([]T, 0)

	for _, value := range values {
		if IsSet(value) {
			list = append(list, value)
		}
	}

	return list
}

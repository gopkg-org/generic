# Generic

This package provides helper functions for generic types.

## Functions

### [Compact](./compact.go) 

Accepts a list and removes entries with empty values.

### [Default](./default.go) 

Default takes two values. 
If the first value is not empty, it will be returned, else the second value will be returned.

### [Has](./has.go)

Has test to see if a list has a particular element.

### [IsSet](./is_set.go)

IsSet check the value and return true if value is non zero.

### [Pointer](./pointer.go)

Pointer return value pointer.

### [Ternary](./ternary.go)

Ternary takes a test boolean value, and two any values.
If the test is true, the first value will be returned.
If the test is empty, the second value will be returned.

### [Uniq](./uniq.go)

Uniq remove duplicate entries in list.

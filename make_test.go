package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestMake(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(MakeSuite))
}

type MakeSuite struct {
	suite.Suite
}

func (suite *MakeSuite) TestSizes() {
	res1 := generic.Make[[]byte]()
	suite.NotNil(res1)
	suite.Equal(0, len(res1))
	suite.Equal(0, cap(res1))

	res2 := generic.Make[[]byte](0)
	suite.NotNil(res2)
	suite.Equal(0, len(res2))
	suite.Equal(0, cap(res2))

	res3 := generic.Make[[]byte](0, 0)
	suite.NotNil(res3)
	suite.Equal(0, len(res3))
	suite.Equal(0, cap(res3))

	res4 := generic.Make[[]byte](0, 1)
	suite.NotNil(res4)
	suite.Equal(0, len(res4))
	suite.Equal(1, cap(res4))

	res5 := generic.Make[[]byte](1)
	suite.NotNil(res5)
	suite.Equal(1, len(res5))
	suite.Equal(1, cap(res5))

	res6 := generic.Make[[]byte](1, 1)
	suite.NotNil(res6)
	suite.Equal(1, len(res6))
	suite.Equal(1, cap(res6))

	res7 := generic.Make[[]byte](1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
	suite.NotNil(res7)
	suite.Equal(1, len(res7))
	suite.Equal(2, cap(res7))
}

func (suite *MakeSuite) TestInvalid() {
	suite.PanicsWithValue("can't make no slice type", func() { generic.Make[string]() })
}

func (suite *MakeSuite) TestBool() {
	suite.Equal([]bool{}, generic.Make[[]bool]())
	suite.Equal([]bool{}, generic.Make[[]bool](0))
	suite.Equal([]bool{}, generic.Make[[]bool](0, 1))
	suite.Equal([]bool{false}, generic.Make[[]bool](1))
	suite.Equal([]bool{false}, generic.Make[[]bool](1, 1))
}

func (suite *MakeSuite) TestUint8() {
	suite.Equal([]uint8{}, generic.Make[[]uint8]())
	suite.Equal([]uint8{}, generic.Make[[]uint8](0))
	suite.Equal([]uint8{}, generic.Make[[]uint8](0, 1))
	suite.Equal([]uint8{0}, generic.Make[[]uint8](1))
	suite.Equal([]uint8{0}, generic.Make[[]uint8](1, 1))
}

func (suite *MakeSuite) TestUint16() {
	suite.Equal([]uint16{}, generic.Make[[]uint16]())
	suite.Equal([]uint16{}, generic.Make[[]uint16](0))
	suite.Equal([]uint16{}, generic.Make[[]uint16](0, 1))
	suite.Equal([]uint16{0}, generic.Make[[]uint16](1))
	suite.Equal([]uint16{0}, generic.Make[[]uint16](1, 1))
}

func (suite *MakeSuite) TestUint32() {
	suite.Equal([]uint32{}, generic.Make[[]uint32]())
	suite.Equal([]uint32{}, generic.Make[[]uint32](0))
	suite.Equal([]uint32{}, generic.Make[[]uint32](0, 1))
	suite.Equal([]uint32{0}, generic.Make[[]uint32](1))
	suite.Equal([]uint32{0}, generic.Make[[]uint32](1, 1))
}

func (suite *MakeSuite) TestUint64() {
	suite.Equal([]uint64{}, generic.Make[[]uint64]())
	suite.Equal([]uint64{}, generic.Make[[]uint64](0))
	suite.Equal([]uint64{}, generic.Make[[]uint64](0, 1))
	suite.Equal([]uint64{0}, generic.Make[[]uint64](1))
	suite.Equal([]uint64{0}, generic.Make[[]uint64](1, 1))
}

func (suite *MakeSuite) TestInt8() {
	suite.Equal([]int8{}, generic.Make[[]int8]())
	suite.Equal([]int8{}, generic.Make[[]int8](0))
	suite.Equal([]int8{}, generic.Make[[]int8](0, 1))
	suite.Equal([]int8{0}, generic.Make[[]int8](1))
	suite.Equal([]int8{0}, generic.Make[[]int8](1, 1))
}

func (suite *MakeSuite) TestInt16() {
	suite.Equal([]int16{}, generic.Make[[]int16]())
	suite.Equal([]int16{}, generic.Make[[]int16](0))
	suite.Equal([]int16{}, generic.Make[[]int16](0, 1))
	suite.Equal([]int16{0}, generic.Make[[]int16](1))
	suite.Equal([]int16{0}, generic.Make[[]int16](1, 1))
}

func (suite *MakeSuite) TestInt32() {
	suite.Equal([]int32{}, generic.Make[[]int32]())
	suite.Equal([]int32{}, generic.Make[[]int32](0))
	suite.Equal([]int32{}, generic.Make[[]int32](0, 1))
	suite.Equal([]int32{0}, generic.Make[[]int32](1))
	suite.Equal([]int32{0}, generic.Make[[]int32](1, 1))
}

func (suite *MakeSuite) TestInt64() {
	suite.Equal([]int64{}, generic.Make[[]int64]())
	suite.Equal([]int64{}, generic.Make[[]int64](0))
	suite.Equal([]int64{}, generic.Make[[]int64](0, 1))
	suite.Equal([]int64{0}, generic.Make[[]int64](1))
	suite.Equal([]int64{0}, generic.Make[[]int64](1, 1))
}

func (suite *MakeSuite) TestFloat32() {
	suite.Equal([]float32{}, generic.Make[[]float32]())
	suite.Equal([]float32{}, generic.Make[[]float32](0))
	suite.Equal([]float32{}, generic.Make[[]float32](0, 1))
	suite.Equal([]float32{0}, generic.Make[[]float32](1))
	suite.Equal([]float32{0}, generic.Make[[]float32](1, 1))
}

func (suite *MakeSuite) TestFloat64() {
	suite.Equal([]float64{}, generic.Make[[]float64]())
	suite.Equal([]float64{}, generic.Make[[]float64](0))
	suite.Equal([]float64{}, generic.Make[[]float64](0, 1))
	suite.Equal([]float64{0}, generic.Make[[]float64](1))
	suite.Equal([]float64{0}, generic.Make[[]float64](1, 1))
}

func (suite *MakeSuite) TestComplex64() {
	suite.Equal([]complex64{}, generic.Make[[]complex64]())
	suite.Equal([]complex64{}, generic.Make[[]complex64](0))
	suite.Equal([]complex64{}, generic.Make[[]complex64](0, 1))
	suite.Equal([]complex64{0}, generic.Make[[]complex64](1))
	suite.Equal([]complex64{0}, generic.Make[[]complex64](1, 1))
}

func (suite *MakeSuite) TestComplex128() {
	suite.Equal([]complex128{}, generic.Make[[]complex128]())
	suite.Equal([]complex128{}, generic.Make[[]complex128](0))
	suite.Equal([]complex128{}, generic.Make[[]complex128](0, 1))
	suite.Equal([]complex128{0}, generic.Make[[]complex128](1))
	suite.Equal([]complex128{0}, generic.Make[[]complex128](1, 1))
}

func (suite *MakeSuite) TestString() {
	suite.Equal([]string{}, generic.Make[[]string]())
	suite.Equal([]string{}, generic.Make[[]string](0))
	suite.Equal([]string{}, generic.Make[[]string](0, 1))
	suite.Equal([]string{""}, generic.Make[[]string](1))
	suite.Equal([]string{""}, generic.Make[[]string](1, 1))
}

func (suite *MakeSuite) TestInt() {
	suite.Equal([]int{}, generic.Make[[]int]())
	suite.Equal([]int{}, generic.Make[[]int](0))
	suite.Equal([]int{}, generic.Make[[]int](0, 1))
	suite.Equal([]int{0}, generic.Make[[]int](1))
	suite.Equal([]int{0}, generic.Make[[]int](1, 1))
}

func (suite *MakeSuite) TestUint() {
	suite.Equal([]uint{}, generic.Make[[]uint]())
	suite.Equal([]uint{}, generic.Make[[]uint](0))
	suite.Equal([]uint{}, generic.Make[[]uint](0, 1))
	suite.Equal([]uint{0}, generic.Make[[]uint](1))
	suite.Equal([]uint{0}, generic.Make[[]uint](1, 1))
}

func (suite *MakeSuite) TestUintPtr() {
	suite.Equal([]uintptr{}, generic.Make[[]uintptr]())
	suite.Equal([]uintptr{}, generic.Make[[]uintptr](0))
	suite.Equal([]uintptr{}, generic.Make[[]uintptr](0, 1))
	suite.Equal([]uintptr{0}, generic.Make[[]uintptr](1))
	suite.Equal([]uintptr{0}, generic.Make[[]uintptr](1, 1))
}

func (suite *MakeSuite) TestByte() {
	suite.Equal([]byte{}, generic.Make[[]byte]())
	suite.Equal([]byte{}, generic.Make[[]byte](0))
	suite.Equal([]byte{}, generic.Make[[]byte](0, 1))
	suite.Equal([]byte{0x0}, generic.Make[[]byte](1))
	suite.Equal([]byte{0x0}, generic.Make[[]byte](1, 1))
}

func (suite *MakeSuite) TestRune() {
	suite.Equal([]rune{}, generic.Make[[]rune]())
	suite.Equal([]rune{}, generic.Make[[]rune](0))
	suite.Equal([]rune{}, generic.Make[[]rune](0, 1))
	suite.Equal([]rune{0x0}, generic.Make[[]rune](1))
	suite.Equal([]rune{0x0}, generic.Make[[]rune](1, 1))
}

func (suite *MakeSuite) TestAny() {
	suite.Equal([]any{}, generic.Make[[]any]())
	suite.Equal([]any{}, generic.Make[[]any](0))
	suite.Equal([]any{}, generic.Make[[]any](0, 1))
	suite.Equal([]any{nil}, generic.Make[[]any](1))
	suite.Equal([]any{nil}, generic.Make[[]any](1, 1))
}

func (suite *MakeSuite) TestStruct() {
	type Struct struct{ Bool bool }

	suite.Equal([]Struct{}, generic.Make[[]Struct]())
	suite.Equal([]Struct{}, generic.Make[[]Struct](0))
	suite.Equal([]Struct{}, generic.Make[[]Struct](0, 1))
	suite.Equal([]Struct{{Bool: false}}, generic.Make[[]Struct](1))
	suite.Equal([]Struct{{Bool: false}}, generic.Make[[]Struct](1, 1))
}

func ExampleMake_bool() {
	fmt.Printf("%#v\n", generic.Make[[]bool]())
	fmt.Printf("%#v\n", generic.Make[[]bool](1))
	// Output:
	// []bool{}
	// []bool{false}
}

func ExampleMake_uint8() {
	fmt.Printf("%#v\n", generic.Make[[]uint8]())
	fmt.Printf("%#v\n", generic.Make[[]uint8](1))
	// Output:
	// []byte{}
	// []byte{0x0}
}

func ExampleMake_uint16() {
	fmt.Printf("%#v\n", generic.Make[[]uint16]())
	fmt.Printf("%#v\n", generic.Make[[]uint16](1))
	// Output:
	// []uint16{}
	// []uint16{0x0}
}

func ExampleMake_uint32() {
	fmt.Printf("%#v\n", generic.Make[[]uint32]())
	fmt.Printf("%#v\n", generic.Make[[]uint32](1))
	// Output:
	// []uint32{}
	// []uint32{0x0}
}

func ExampleMake_uint64() {
	fmt.Printf("%#v\n", generic.Make[[]uint64]())
	fmt.Printf("%#v\n", generic.Make[[]uint64](1))
	// Output:
	// []uint64{}
	// []uint64{0x0}
}

func ExampleMake_int8() {
	fmt.Printf("%#v\n", generic.Make[[]int8]())
	fmt.Printf("%#v\n", generic.Make[[]int8](1))
	// Output:
	// []int8{}
	// []int8{0}
}

func ExampleMake_int16() {
	fmt.Printf("%#v\n", generic.Make[[]int16]())
	fmt.Printf("%#v\n", generic.Make[[]int16](1))
	// Output:
	// []int16{}
	// []int16{0}
}

func ExampleMake_int32() {
	fmt.Printf("%#v\n", generic.Make[[]int32]())
	fmt.Printf("%#v\n", generic.Make[[]int32](1))
	// Output:
	// []int32{}
	// []int32{0}
}

func ExampleMake_int64() {
	fmt.Printf("%#v\n", generic.Make[[]int64]())
	fmt.Printf("%#v\n", generic.Make[[]int64](1))
	// Output:
	// []int64{}
	// []int64{0}
}

func ExampleMake_float32() {
	fmt.Printf("%#v\n", generic.Make[[]float32]())
	fmt.Printf("%#v\n", generic.Make[[]float32](1))
	// Output:
	// []float32{}
	// []float32{0}
}

func ExampleMake_float64() {
	fmt.Printf("%#v\n", generic.Make[[]float64]())
	fmt.Printf("%#v\n", generic.Make[[]float64](1))
	// Output:
	// []float64{}
	// []float64{0}
}

func ExampleMake_complex64() {
	fmt.Printf("%#v\n", generic.Make[[]complex64]())
	fmt.Printf("%#v\n", generic.Make[[]complex64](1))
	// Output:
	// []complex64{}
	// []complex64{(0+0i)}
}

func ExampleMake_complex128() {
	fmt.Printf("%#v\n", generic.Make[[]complex128]())
	fmt.Printf("%#v\n", generic.Make[[]complex128](1))
	// Output:
	// []complex128{}
	// []complex128{(0+0i)}
}

func ExampleMake_string() {
	fmt.Printf("%#v\n", generic.Make[[]string]())
	fmt.Printf("%#v\n", generic.Make[[]string](1))
	// Output:
	// []string{}
	// []string{""}
}

func ExampleMake_int() {
	fmt.Printf("%#v\n", generic.Make[[]int]())
	fmt.Printf("%#v\n", generic.Make[[]int](1))
	// Output:
	// []int{}
	// []int{0}
}

func ExampleMake_uint() {
	fmt.Printf("%#v\n", generic.Make[[]uint]())
	fmt.Printf("%#v\n", generic.Make[[]uint](1))
	// Output:
	// []uint{}
	// []uint{0x0}
}

func ExampleMake_uintptr() {
	fmt.Printf("%#v\n", generic.Make[[]uintptr]())
	fmt.Printf("%#v\n", generic.Make[[]uintptr](1))
	// Output:
	// []uintptr{}
	// []uintptr{0x0}
}

func ExampleMake_byte() {
	fmt.Printf("%#v\n", generic.Make[[]byte]())
	fmt.Printf("%#v\n", generic.Make[[]byte](1))
	// Output:
	// []byte{}
	// []byte{0x0}
}

func ExampleMake_rune() {
	fmt.Printf("%#v\n", generic.Make[[]rune]())
	fmt.Printf("%#v\n", generic.Make[[]rune](1))
	// Output:
	// []int32{}
	// []int32{0}
}

func ExampleMake_any() {
	fmt.Printf("%#v\n", generic.Make[[]any]())
	fmt.Printf("%#v\n", generic.Make[[]any](1))
	// Output:
	// []interface {}{}
	// []interface {}{interface {}(nil)}
}

func ExampleMake_struct() {
	type Struct struct{ Bool bool }

	fmt.Printf("%#v\n", generic.Make[[]Struct]())
	fmt.Printf("%#v\n", generic.Make[[]Struct](1))
	// Output:
	// []generic_test.Struct{}
	// []generic_test.Struct{generic_test.Struct{Bool:false}}
}

package generic

import "reflect"

// IsSet check the value and return true if value is non zero.
func IsSet[T any](v T) bool {
	vo := reflect.ValueOf(v)

	return !reflect.DeepEqual(vo.Interface(), reflect.Zero(vo.Type()).Interface())
}

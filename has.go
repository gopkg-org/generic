package generic

// Has test to see if a list has a particular element.
func Has[T comparable](value T, values ...T) bool {
	if values == nil {
		return false
	}

	for _, v := range values {
		if v == value {
			return true
		}
	}

	return false
}

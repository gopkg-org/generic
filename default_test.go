package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestDefault(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(DefaultSuite))
}

type DefaultSuite struct {
	suite.Suite
}

func (suite *DefaultSuite) TestBytes() {
	suite.Equal(byte(0x79), generic.Default(byte(0x79), byte(0x6e)))
	suite.Equal(byte(0x6e), generic.Default(byte(0x00), byte(0x6e)))
}

func (suite *DefaultSuite) TestComplex64() {
	suite.Equal(complex64(1.9), generic.Default(complex64(1.9), complex64(2.8)))
	suite.Equal(complex64(2.8), generic.Default(complex64(0.0), complex64(2.8)))
}

func (suite *DefaultSuite) TestComplex128() {
	suite.Equal(complex128(1.9), generic.Default(complex128(1.9), complex128(2.8)))
	suite.Equal(complex128(2.8), generic.Default(complex128(0.0), complex128(2.8)))
}

func (suite *DefaultSuite) TestFloat32() {
	suite.Equal(float32(1.9), generic.Default(float32(1.9), float32(2.8)))
	suite.Equal(float32(2.8), generic.Default(float32(0.0), float32(2.8)))
}

func (suite *DefaultSuite) TestFloat64() {
	suite.Equal(1.9, generic.Default(1.9, 2.8))
	suite.Equal(2.8, generic.Default(0.0, 2.8))
}

func (suite *DefaultSuite) TestInt() {
	suite.Equal(1, generic.Default(1, 2))
	suite.Equal(2, generic.Default(0, 2))
}

func (suite *DefaultSuite) TestInt8() {
	suite.Equal(int8(1), generic.Default(int8(1), int8(2)))
	suite.Equal(int8(2), generic.Default(int8(0), int8(2)))
}

func (suite *DefaultSuite) TestInt16() {
	suite.Equal(int16(1), generic.Default(int16(1), int16(2)))
	suite.Equal(int16(2), generic.Default(int16(0), int16(2)))
}

func (suite *DefaultSuite) TestInt32() {
	suite.Equal(int32(1), generic.Default(int32(1), int32(2)))
	suite.Equal(int32(2), generic.Default(int32(0), int32(2)))
}

func (suite *DefaultSuite) TestInt64() {
	suite.Equal(int64(1), generic.Default(int64(1), int64(2)))
	suite.Equal(int64(2), generic.Default(int64(0), int64(2)))
}

func (suite *DefaultSuite) TestRunes() {
	suite.Equal(rune(0x79), generic.Default(rune(0x79), rune(0x6e)))
	suite.Equal(rune(0x6e), generic.Default(rune(0x00), rune(0x6e)))
}

func (suite *DefaultSuite) TestStrings() {
	suite.Equal("value1", generic.Default("value1", "value2"))
	suite.Equal("value2", generic.Default("", "value2"))
}

func (suite *DefaultSuite) TestUint() {
	suite.Equal(uint(1), generic.Default(uint(1), uint(2)))
	suite.Equal(uint(2), generic.Default(uint(0), uint(2)))
}

func (suite *DefaultSuite) TestUint8() {
	suite.Equal(uint8(1), generic.Default(uint8(1), uint8(2)))
	suite.Equal(uint8(2), generic.Default(uint8(0), uint8(2)))
}

func (suite *DefaultSuite) TestUint16() {
	suite.Equal(uint16(1), generic.Default(uint16(1), uint16(2)))
	suite.Equal(uint16(2), generic.Default(uint16(0), uint16(2)))
}

func (suite *DefaultSuite) TestUint32() {
	suite.Equal(uint32(1), generic.Default(uint32(1), uint32(2)))
	suite.Equal(uint32(2), generic.Default(uint32(0), uint32(2)))
}

func (suite *DefaultSuite) TestUint64() {
	suite.Equal(uint64(1), generic.Default(uint64(1), uint64(2)))
	suite.Equal(uint64(2), generic.Default(uint64(0), uint64(2)))
}

func (suite *DefaultSuite) TestUintPtr() {
	suite.Equal(uintptr(1), generic.Default(uintptr(1), uintptr(2)))
	suite.Equal(uintptr(2), generic.Default(uintptr(0), uintptr(2)))
}

func ExampleDefault_bytes() {
	fmt.Printf("%#x\n", generic.Default(byte(0x79), byte(0x6e)))
	fmt.Printf("%#x\n", generic.Default(byte(0x00), byte(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleDefault_complex64() {
	fmt.Printf("%#v\n", generic.Default(complex64(1.9), complex64(2.8)))
	fmt.Printf("%#v\n", generic.Default(complex64(0.0), complex64(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleDefault_complex128() {
	fmt.Printf("%#v\n", generic.Default(complex128(1.9), complex128(2.8)))
	fmt.Printf("%#v\n", generic.Default(complex128(0.0), complex128(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleDefault_float32() {
	fmt.Printf("%#v\n", generic.Default(float32(1.9), float32(2.8)))
	fmt.Printf("%#v\n", generic.Default(float32(0.0), float32(2.8)))
	// Output:
	// 1.9
	// 2.8
}

func ExampleDefault_float64() {
	fmt.Printf("%#v\n", generic.Default(1.9, 2.8))
	fmt.Printf("%#v\n", generic.Default(0.0, 2.8))
	// Output:
	// 1.9
	// 2.8
}

func ExampleDefault_int() {
	fmt.Printf("%v\n", generic.Default(1, 2))
	fmt.Printf("%v\n", generic.Default(0, 2))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int8() {
	fmt.Printf("%v\n", generic.Default(int8(1), int8(2)))
	fmt.Printf("%v\n", generic.Default(int8(0), int8(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int16() {
	fmt.Printf("%v\n", generic.Default(int16(1), int16(2)))
	fmt.Printf("%v\n", generic.Default(int16(0), int16(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int32() {
	fmt.Printf("%v\n", generic.Default(int32(1), int32(2)))
	fmt.Printf("%v\n", generic.Default(int32(0), int32(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_int64() {
	fmt.Printf("%v\n", generic.Default(int64(1), int64(2)))
	fmt.Printf("%v\n", generic.Default(int64(0), int64(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_runes() {
	fmt.Printf("%#x\n", generic.Default(rune(0x79), rune(0x6e)))
	fmt.Printf("%#x\n", generic.Default(rune(0x00), rune(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleDefault_strings() {
	fmt.Printf("%s\n", generic.Default("value1", "value2"))
	fmt.Printf("%s\n", generic.Default("", "value2"))
	// Output:
	// value1
	// value2
}

func ExampleDefault_uint() {
	fmt.Printf("%v\n", generic.Default(uint(1), uint(2)))
	fmt.Printf("%v\n", generic.Default(uint(0), uint(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint8() {
	fmt.Printf("%v\n", generic.Default(uint8(1), uint8(2)))
	fmt.Printf("%v\n", generic.Default(uint8(0), uint8(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint16() {
	fmt.Printf("%v\n", generic.Default(uint16(1), uint16(2)))
	fmt.Printf("%v\n", generic.Default(uint16(0), uint16(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint32() {
	fmt.Printf("%v\n", generic.Default(uint32(1), uint32(2)))
	fmt.Printf("%v\n", generic.Default(uint32(0), uint32(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uint64() {
	fmt.Printf("%v\n", generic.Default(uint64(1), uint64(2)))
	fmt.Printf("%v\n", generic.Default(uint64(0), uint64(2)))
	// Output:
	// 1
	// 2
}

func ExampleDefault_uintPtr() {
	fmt.Printf("%v\n", generic.Default(uintptr(1), uintptr(2)))
	fmt.Printf("%v\n", generic.Default(uintptr(0), uintptr(2)))
	// Output:
	// 1
	// 2
}

package generic_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/generic"
)

func TestTernary(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(TernarySuite))
}

type TernarySuite struct {
	suite.Suite
}

func (suite *TernarySuite) TestBytes() {
	suite.Equal(byte(0x79), generic.Ternary(true, byte(0x79), byte(0x6e)))
	suite.Equal(byte(0x6e), generic.Ternary(false, byte(0x79), byte(0x6e)))
}

func (suite *TernarySuite) TestComplex64() {
	suite.Equal(complex64(1.9), generic.Ternary(true, complex64(1.9), complex64(2.8)))
	suite.Equal(complex64(2.8), generic.Ternary(false, complex64(1.9), complex64(2.8)))
}

func (suite *TernarySuite) TestComplex128() {
	suite.Equal(complex128(1.9), generic.Ternary(true, complex128(1.9), complex128(2.8)))
	suite.Equal(complex128(2.8), generic.Ternary(false, complex128(1.9), complex128(2.8)))
}

func (suite *TernarySuite) TestFloat32() {
	suite.Equal(float32(1.9), generic.Ternary(true, float32(1.9), float32(2.8)))
	suite.Equal(float32(2.8), generic.Ternary(false, float32(1.9), float32(2.8)))
}

func (suite *TernarySuite) TestFloat64() {
	suite.Equal(float64(1.9), generic.Ternary(true, float64(1.9), float64(2.8)))
	suite.Equal(float64(2.8), generic.Ternary(false, float64(1.9), float64(2.8)))
}

func (suite *TernarySuite) TestInt() {
	suite.Equal(1, generic.Ternary(true, 1, 2))
	suite.Equal(2, generic.Ternary(false, 1, 2))
}

func (suite *TernarySuite) TestInt8() {
	suite.Equal(int8(1), generic.Ternary(true, int8(1), int8(2)))
	suite.Equal(int8(2), generic.Ternary(false, int8(1), int8(2)))
}

func (suite *TernarySuite) TestInt16() {
	suite.Equal(int16(1), generic.Ternary(true, int16(1), int16(2)))
	suite.Equal(int16(2), generic.Ternary(false, int16(1), int16(2)))
}

func (suite *TernarySuite) TestInt32() {
	suite.Equal(int32(1), generic.Ternary(true, int32(1), int32(2)))
	suite.Equal(int32(2), generic.Ternary(false, int32(1), int32(2)))
}

func (suite *TernarySuite) TestInt64() {
	suite.Equal(int64(1), generic.Ternary(true, int64(1), int64(2)))
	suite.Equal(int64(2), generic.Ternary(false, int64(1), int64(2)))
}

func (suite *TernarySuite) TestRunes() {
	suite.Equal(rune(0x79), generic.Ternary(true, rune(0x79), rune(0x6e)))
	suite.Equal(rune(0x6e), generic.Ternary(false, rune(0x79), rune(0x6e)))
}

func (suite *TernarySuite) TestStrings() {
	suite.Equal("True", generic.Ternary(true, "True", "False"))
	suite.Equal("False", generic.Ternary(false, "True", "False"))
}

func (suite *TernarySuite) TestUint() {
	suite.Equal(uint(1), generic.Ternary(true, uint(1), uint(2)))
	suite.Equal(uint(2), generic.Ternary(false, uint(1), uint(2)))
}

func (suite *TernarySuite) TestUint8() {
	suite.Equal(uint8(1), generic.Ternary(true, uint8(1), uint8(2)))
	suite.Equal(uint8(2), generic.Ternary(false, uint8(1), uint8(2)))
}

func (suite *TernarySuite) TestUint16() {
	suite.Equal(uint16(1), generic.Ternary(true, uint16(1), uint16(2)))
	suite.Equal(uint16(2), generic.Ternary(false, uint16(1), uint16(2)))
}

func (suite *TernarySuite) TestUint32() {
	suite.Equal(uint32(1), generic.Ternary(true, uint32(1), uint32(2)))
	suite.Equal(uint32(2), generic.Ternary(false, uint32(1), uint32(2)))
}

func (suite *TernarySuite) TestUint64() {
	suite.Equal(uint64(1), generic.Ternary(true, uint64(1), uint64(2)))
	suite.Equal(uint64(2), generic.Ternary(false, uint64(1), uint64(2)))
}

func (suite *TernarySuite) TestUintPtr() {
	suite.Equal(uintptr(1), generic.Ternary(true, uintptr(1), uintptr(2)))
	suite.Equal(uintptr(2), generic.Ternary(false, uintptr(1), uintptr(2)))
}

func ExampleTernary_bytes() {
	fmt.Printf("%#x\n", generic.Ternary(true, byte(0x79), byte(0x6e)))
	fmt.Printf("%#x\n", generic.Ternary(false, byte(0x79), byte(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleTernary_complex64() {
	fmt.Printf("%#v\n", generic.Ternary(true, complex64(1.9), complex64(2.8)))
	fmt.Printf("%#v\n", generic.Ternary(false, complex64(1.9), complex64(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleTernary_complex128() {
	fmt.Printf("%#v\n", generic.Ternary(true, complex128(1.9), complex128(2.8)))
	fmt.Printf("%#v\n", generic.Ternary(false, complex128(1.9), complex128(2.8)))
	// Output:
	// (1.9+0i)
	// (2.8+0i)
}

func ExampleTernary_float32() {
	fmt.Printf("%#v\n", generic.Ternary(true, float32(1.9), float32(2.8)))
	fmt.Printf("%#v\n", generic.Ternary(false, float32(1.9), float32(2.8)))
	// Output:
	// 1.9
	// 2.8
}

func ExampleTernary_float64() {
	fmt.Printf("%#v\n", generic.Ternary(true, 1.9, 2.8))
	fmt.Printf("%#v\n", generic.Ternary(false, 1.9, 2.8))
	// Output:
	// 1.9
	// 2.8
}

func ExampleTernary_int() {
	fmt.Printf("%v\n", generic.Ternary(true, 1, 2))
	fmt.Printf("%v\n", generic.Ternary(false, 1, 2))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int8() {
	fmt.Printf("%v\n", generic.Ternary(true, int8(1), int8(2)))
	fmt.Printf("%v\n", generic.Ternary(false, int8(1), int8(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int16() {
	fmt.Printf("%v\n", generic.Ternary(true, int16(1), int16(2)))
	fmt.Printf("%v\n", generic.Ternary(false, int16(1), int16(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int32() {
	fmt.Printf("%v\n", generic.Ternary(true, int32(1), int32(2)))
	fmt.Printf("%v\n", generic.Ternary(false, int32(1), int32(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_int64() {
	fmt.Printf("%v\n", generic.Ternary(true, int64(1), int64(2)))
	fmt.Printf("%v\n", generic.Ternary(false, int64(1), int64(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_runes() {
	fmt.Printf("%#x\n", generic.Ternary(true, rune(0x79), rune(0x6e)))
	fmt.Printf("%#x\n", generic.Ternary(false, rune(0x79), rune(0x6e)))
	// Output:
	// 0x79
	// 0x6e
}

func ExampleTernary_strings() {
	fmt.Printf("%s\n", generic.Ternary(true, "True", "False"))
	fmt.Printf("%s\n", generic.Ternary(false, "True", "False"))
	// Output:
	// True
	// False
}

func ExampleTernary_uint() {
	fmt.Printf("%v\n", generic.Ternary(true, uint(1), uint(2)))
	fmt.Printf("%v\n", generic.Ternary(false, uint(1), uint(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint8() {
	fmt.Printf("%v\n", generic.Ternary(true, uint8(1), uint8(2)))
	fmt.Printf("%v\n", generic.Ternary(false, uint8(1), uint8(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint16() {
	fmt.Printf("%v\n", generic.Ternary(true, uint16(1), uint16(2)))
	fmt.Printf("%v\n", generic.Ternary(false, uint16(1), uint16(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint32() {
	fmt.Printf("%v\n", generic.Ternary(true, uint32(1), uint32(2)))
	fmt.Printf("%v\n", generic.Ternary(false, uint32(1), uint32(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uint64() {
	fmt.Printf("%v\n", generic.Ternary(true, uint64(1), uint64(2)))
	fmt.Printf("%v\n", generic.Ternary(false, uint64(1), uint64(2)))
	// Output:
	// 1
	// 2
}

func ExampleTernary_uintPtr() {
	fmt.Printf("%v\n", generic.Ternary(true, uintptr(1), uintptr(2)))
	fmt.Printf("%v\n", generic.Ternary(false, uintptr(1), uintptr(2)))
	// Output:
	// 1
	// 2
}

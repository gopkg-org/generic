module gopkg.org/generic

go 1.20

require github.com/stretchr/testify v1.8.4

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

retract (
	v1.0.0 // Incorrect result for IsSet function name
	v1.0.1 // IsSet tests not accepeted
)

package generic

import "reflect"

func Zero[T any]() T {
	var t T
	return reflect.Zero(reflect.TypeOf(t)).Interface().(T)
}
